 # 1. Prohledávání stavového prostoru

Úkol je implementován v programovacím jazyce python.

Jednotlivé algoritmy se spuští z mainu. Po jejich doběhnutí vyskočí okno se závěrem. Po odkliknutí se provede další algoritmus.

Na začátku **maze.py** lze upravit prostředí.

- Zobrazení, respektive velikost animace pomocí proměnné *SCALE* - čím větší hodnota, tím větší animace.
- Zpomalení vykreslování se provede zvýšením proměnné *SLOW*, je to implementováno pomocí sleep(SLOW), takže 0.01 je dost velký zpomalení
- Proměnná *input* nastavuje cestu k textové předloze bludiště

 Jsou využity následující knihovny
 ```python
import tkinter import Tk, Canvas, messagebox
import time import sleep
from collections import deque
import re
import sys
import random
from heapq import heappop, heappush
```
