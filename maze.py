from tkinter import Tk, Canvas, messagebox
from time import sleep
from collections import deque
import re
import sys
import random
from heapq import heappop, heappush, heapify


# input="./dataset/114.txt"
# input="./dataset/00_11_11_1550177690.txt"
input="./dataset/01_71_51_156.txt"
# input="./dataset/02_71_51_1552235384.txt"

scale=5
slow=0

def getInts(line):
    return [int(i) for i in re.findall('\d+', line)]

with open(input, 'r') as file:
        MAZE = file.read().split('\n')

if(MAZE[-1] == ""): #soubor konci prazdnou radkou
    del MAZE[-1]

END = tuple(getInts(MAZE[-1]))
del MAZE[-1]
START = tuple(getInts(MAZE[-1]))
del MAZE[-1]

master = Tk()
master.title("Maze")
w = Canvas(master, width=len(MAZE[0])*scale, height=len(MAZE)*scale)
w.config(background='white')
w.pack()

def printRec(x, y, col="black", stip=""):
    w.create_rectangle(x*scale, y*scale, 
                       (x*scale)+scale, (y*scale)+scale, 
                       fill=col, outline=col, stipple=stip)
    w.update()

def printWalls():
    for row in range(len(MAZE)):
        for col in range(len(MAZE[0])):
            if(MAZE[row][col] == "X"):
                w.create_rectangle(col*scale, row*scale, 
                       (col*scale)+scale, (row*scale)+scale, 
                       fill="black", outline="black")
    w.update()

def showPath(parent):
    printRec(END[0], END[1], "blue")
    path = (END)
    length = 0

    while parent[path] != (None, None):
        length = length+1
        path = parent[path]
        printRec(path[0], path[1], "yellow")
        sleep(slow)
    printRec(START[0], START[1], "green")
    
    messagebox.showinfo("Result", "Path length: " + str(length) + "\nNodes expanded: " + str(len(parent)-1))
    return True

def isValid(pos):
    x = pos[0]
    y = pos[1]

    if( x < 0 or x >= len(MAZE[0])):
        return False
    if( y < 0 or y > len(MAZE)):
        return False
    if( MAZE[y][x] == "X" ):
        return False
    
    return True

def bfs():
    parent = {}
    q = deque()
    q.append(START)
    parent[START] = (None, None)

    while(q):
        x, y = q.popleft()
        if((x,y) == END):
            return showPath(parent)

        for pos in [(x-1, y), (x+1, y), (x, y-1), (x, y+1)]:
            if( isValid(pos) and pos not in parent):
                parent[pos] = (x,y)
                printRec(pos[0], pos[1], "red")
                q.append(pos)
                sleep(slow)

def dfs():
    parent = {}
    s = deque()
    s.append(START)
    parent[START] = (None, None)

    while(s):
        x, y = s.pop()
        if((x,y) == END):
            return showPath(parent)

        for pos in [(x-1, y), (x+1, y), (x, y-1), (x, y+1)]:
            if( isValid(pos) and pos not in parent):
                parent[pos] = (x,y)
                printRec(pos[0], pos[1], "red")
                s.append(pos)
                sleep(slow)

def randomSearch():

    parent = {}
    o = [START]
    parent[START] = (None, None)

    while(o):
        x, y = o.pop(random.randrange(len(o)))
        if((x,y) == END):
            return showPath(parent)
 
        for pos in [(x-1, y), (x+1, y), (x, y-1), (x, y+1)]:
            if( isValid(pos) and pos not in parent):
                parent[pos] = (x,y)
                printRec(pos[0], pos[1], "red")
                o.append(pos)
                sleep(slow)

def getNearest(s, dist):
    minim = list(s)[0]
    for i in s:
        if(dist[i] < dist[minim]):
            minim = i

    return minim

def dijkstra():

    parent = {}
    dist = {}

    s = set()

    for y in range(len(MAZE)):
        for x in range(len(MAZE[0])):
            dist[x,y] = len(MAZE)*len(MAZE[0]) # "NEKONECNO"
            s.add((x,y))
    
    parent[START] = (None, None)
    dist[START] = 0

    while(s):
        x, y = getNearest(s, dist)
        s.remove((x,y))
        if((x,y) == END):
            return showPath(parent)

        for pos in [(x-1, y), (x+1, y), (x, y-1), (x, y+1)]:
            if( isValid(pos) and 
                (pos not in parent or dist[pos] > dist[x,y]+1) ):
                    parent[pos] = (x,y)
                    dist[pos] = dist[x,y]+1
                    printRec(pos[0], pos[1], "red")
                    sleep(slow)

# Manhattan distance
def heuristic(pos): 
    return int(abs(pos[0] - END[0]) + abs(pos[1] - END[1]))

def greedy():

    parent = {}
    q = []

    parent[START] = (None, None)
    heappush(q, (heuristic(START), START))

    while(q):
        h, xy = heappop(q)
        x, y = xy
        if(xy == END):
            return showPath(parent)
        for pos in [(x-1, y), (x+1, y), (x, y-1), (x, y+1)]:
            if( isValid(pos) and pos not in parent ):
                parent[pos] = (x,y)
                heappush(q, (heuristic(pos), pos))
                printRec(pos[0], pos[1], "red")
                sleep(slow)

def astar():

    parent = {}
    q = []

    parent[START] = (None, None)
    heappush(q, (heuristic(START), 0, START))

    while(q):
        h, dist, xy = heappop(q)
        x, y = xy
        if(xy == END):
            return showPath(parent)
        for pos in [(x-1, y), (x+1, y), (x, y-1), (x, y+1)]:
            if( isValid(pos) and pos not in parent ):
                parent[pos] = (x,y)
                heappush(q, (dist+heuristic(pos), dist+1, pos))
                printRec(pos[0], pos[1], "red")
                sleep(slow)

def newMaze():
    w.create_rectangle(0, 0, 
                       len(MAZE[0])*scale, len(MAZE)*scale, 
                       fill="white", outline="white")
    w.update()
    printWalls()
    printRec(START[0], START[1], "green")
    printRec(END[0], END[1], "blue")

def main():

    printWalls()
    printRec(START[0], START[1], "green")
    printRec(END[0], END[1], "blue")

    print("Random Search")
    randomSearch()
    newMaze()
    print("Breadth-first search")
    bfs()
    newMaze()
    print("Depth-first search")
    dfs()
    newMaze()
    print("Dijkstra's algorithm")
    dijkstra()
    newMaze()
    print("Greedy Search")
    greedy()
    newMaze()
    print("A* Search Algorithm")
    astar()
 
    master.mainloop()



if __name__ == '__main__':
    main()
